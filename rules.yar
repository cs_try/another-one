rule DetectAntiHooking
{
    meta:
        description = "Detects a method for anti-hooking"
        author = "Your Name"
        date = "2024-05-27"
    
    strings:
        $shutil_copy = "shutil.copy" ascii
        $tempfile_gettempdir = "tempfile.gettempdir" ascii
        $ntdll_path = "os.path.join(os.getenv('SystemRoot'), 'System32', 'ntdll.dll')" ascii
        $temp_file_path = "os.path.join(temp_dir, f\"{hardcoded_name}.tmp\")" ascii
        $ctypes_windll_ntdll = "ctypes.windll.ntdll" ascii
        $ctypes_winfunctype = "ctypes.WINFUNCTYPE(NTSTATUS, wintypes.LPWSTR, wintypes.DWORD, wintypes.PVOID, ctypes.POINTER(wintypes.HANDLE))" ascii
    
    condition:
        all of them
}

rule DetectDisablingTaskManager
{
    meta:
        description = "Detects specific Python code patterns related to disabling Task Manager"
        author = "Your Name"
        date = "2024-05-27"
    
    strings:
        $open_key = "reg.OpenKey" ascii
        $set_value_ex = "reg.SetValueEx(key, 'DisableTaskMgr', 0, reg.REG_DWORD, 1)" ascii
        $close_key = "reg.CloseKey(key)" ascii
    
    condition:
        all of them
}

rule DetectHiddenFileAttributesSetting
{
    meta:
        description = "Detects specific Python code patterns that use ctypes to set file attributes to hidden"
        author = "Your Name"
        date = "2024-05-27"
    
    strings:
        $set_file_attributes = "ctypes.windll.kernel32.SetFileAttributesW" ascii
        $hidden_attribute = "0x02" ascii
    
    condition:
        $set_file_attributes and $hidden_attribute
}

rule DetectHeadlessChromeWebDriver
{
    meta:
        description = "Detects specific Python code patterns related to setting up a headless Chrome WebDriver"
        author = "Your Name"
        date = "2024-05-27"
    
    strings:
        $headless = ".headless = True" ascii
        $options = "Options()" ascii
        $webdriver_chrome = "webdriver.Chrome(options=" ascii
    
    condition:
        all of them
}

rule DetectScreenCaptureMethods
{
    meta:
        description = "Detects specific Python code patterns related to screen capture using ImageGrab, mss, or pyautogui"
        author = "Your Name"
        date = "2024-05-27"
    
    strings:
        $imagegrab_grab = "ImageGrab.grab()" ascii
        $mss_shot = ".shot(" ascii
        $pyautogui_screenshot = "pyautogui.screenshot()" ascii
    
    condition:
        any of them
}

rule DetectPlatformUsage
{
    meta:
        description = "Detects the usage of platform module functions to get system information"
        author = "Your Name"
        date = "2024-05-27"
    
    strings:
        $platform_processor = "platform.processor()" ascii
        $platform_machine = "platform.machine()" ascii
        $platform_version = "platform.version()" ascii
        $platform_release = "platform.release()" ascii
        $platform_system = "platform.system()" ascii
    
    condition:
        any of ($platform_processor, $platform_machine, $platform_version, $platform_release, $platform_system)
}

rule DetectAntivirusWMIUsage
{
    meta:
        description = "Detects specific Python code patterns related to querying WMI for antivirus products"
        author = "Your Name"
        date = "2024-05-27"
    
    strings:
        $wmi_WMI = "wmi.WMI()" ascii
        $win32_product = ".Win32_Product()" ascii
        $winmgmts = "win32com.client.GetObject('winmgmts:')" ascii
        $exec_query = ".ExecQuery('SELECT * FROM Win32_Product WHERE Name LIKE \"%antivirus%\"')" ascii
    
    condition:
        ($wmi_WMI and $win32_product) or ($winmgmts and $exec_query)
}

rule DetectDiskUsageFunctions
{
    meta:
        description = "Detects the usage of os.statvfs('/') or psutil.disk_usage('/') to check disk usage"
        author = "Your Name"
        date = "2024-05-27"
    
    strings:
        $os_statvfs = "os.statvfs('/')" ascii
        $psutil_disk_usage = "psutil.disk_usage('/')" ascii
    
    condition:
        any of them
}

rule DetectGetForegroundWindowUsage
{
    meta:
        description = "Detects the usage of user32.GetForegroundWindow and ctypes.windll.user32 in Python code"
        author = "Your Name"
        date = "2024-05-27"
    
    strings:
        $get_foreground_window = "user32.GetForegroundWindow()" ascii
        $ctypes_user32 = "ctypes.windll.user32" ascii
    
    condition:
        any of them
}

rule DetectCriticalProcessManipulation
{
    meta:
        description = "Detects the usage of .RtlSetProcessIsCritical, ctypes.WinDLL('ntdll'), and ctypes.windll.kernel32.GetCurrentProcess() in Python code"
        author = "Your Name"
        date = "2024-05-27"
    
    strings:
        $set_process_critical = ".RtlSetProcessIsCritical" ascii
        $ntdll_import = "ctypes.WinDLL('ntdll')" ascii
        $get_current_process = "ctypes.windll.kernel32.GetCurrentProcess()" ascii
    
    condition:
        any of them
}

rule DetectThreadExecutionStateManipulation
{
    meta:
        description = "Detects the usage of ctypes.windll.kernel32 and SetThreadExecutionState in Python code"
        author = "Your Name"
        date = "2024-05-27"
    
    strings:
        $kernel32_import = "ctypes.windll.kernel32" ascii
        $set_thread_execution_state = "SetThreadExecutionState" ascii
    
    condition:
        any of them
}

rule DetectRegistryKeyUsage
{
    meta:
        description = "Detects the usage of specific registry keys and search strings in Python code"
        author = "Your Name"
        date = "2024-05-27"
    
    strings:
        $registry_keys = "System\\CurrentControlSet\\Enum\\IDE" wide ascii
        $scsi_key = "System\\CurrentControlSet\\Enum\\SCSI" wide ascii
        $search_strings1 = "qemu" wide ascii nocase 
        $search_strings2 = "virtio" wide ascii nocase 
        $search_strings3 = "vmware" wide ascii nocase
        $search_strings4 = "vbox" wide ascii nocase
        $search_strings5 = "xen" wide ascii nocase           
                          
        $open_key = "winreg.OpenKey" wide ascii
        $hkey_local_machine = "winreg.HKEY_LOCAL_MACHINE" wide ascii
       
        $query_info_key = "winreg.QueryInfoKey" wide ascii

    condition:
        ($registry_keys or $scsi_key) and
        all of ($search_strings*) and
        ($open_key or $hkey_local_machine or $query_info_key)
}

rule DetectRundll32
{
    meta:
        description = "Detects the presence of 'rundll32' in a file"
        author = "ChatGPT"
        date = "2024-05-27"
    strings:
        $rundll32_string = "rundll32" ascii
    condition:
        $rundll32_string
}
