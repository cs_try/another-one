import os
import yara
import json
import sys

# Compile YARA rules once for reuse
rules = yara.compile(filepath="rules.yar")

def scan_file_for_rules(file_path):
    matched_rules = []

    # Scan the file with compiled YARA rules
    matches = rules.match(file_path)

    # Get descriptions of matching rules
    for match in matches:
        description = match.meta.get('description', 'No description available')
        matched_rules.append(description)

    return matched_rules

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python scan_for_yara_rules.py <file_path>")
        sys.exit(1)

    file_path = sys.argv[1]

    matched_rules = scan_file_for_rules(file_path)

    if matched_rules:
        # Create a dictionary to store the results
        results = {
            "file_path": file_path,
            "matched_descriptions": matched_rules
        }

        # Ensure the results directory exists
        os.makedirs('results', exist_ok=True)

        # Save results to a JSON file
        output_file = "results/scan_results.json"
        
        # Append results to the JSON file with proper comma handling
        if os.path.exists(output_file) and os.path.getsize(output_file) > 0:
            with open(output_file, "r+") as json_file:
                json_file.seek(0, os.SEEK_END)
                json_file.seek(json_file.tell() - 1, os.SEEK_SET)
                json_file.write(",\n")
                json_file.write(json.dumps(results, indent=4))
                json_file.write("\n]")
        else:
            with open(output_file, "w") as json_file:
                json_file.write("[\n")
                json.dump(results, json_file, indent=4)
                json_file.write("\n]")

        print(f"Matched rules for {file_path}:")
        for rule in matched_rules:
            print("- ", rule)
    else:
        print(f"No rules matched for {file_path}.")
