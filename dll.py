import subprocess

# Define the DLL and the function you want to call
dll_path = "path_to_your_dll.dll"
function_name = "YourFunctionName"

# Construct the rundll32 command
command = f"rundll32 {dll_path},{function_name}"

# Run the command using subprocess
result = subprocess.run(command, shell=True, capture_output=True, text=True)

# Print the output
print("Output:", result.stdout)
print("Error:", result.stderr)
print("Return Code:", result.returncode)
